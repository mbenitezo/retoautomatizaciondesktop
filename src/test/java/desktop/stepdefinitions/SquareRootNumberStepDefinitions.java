package desktop.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import desktop.questions.TheResult;
import desktop.tasks.Calculate;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.core.Is.is;

import org.openqa.selenium.WebDriver;

public class SquareRootNumberStepDefinitions {

	@Managed
    private WebDriver driver;
    private Actor Anacleto = Actor.named("Anacleto");

    @Before
    public void prepareStage() {
        Anacleto.can(BrowseTheWeb.with(driver));
    }
	
	@When("^I calculate square root of (\\d+)$")
	public void i_calculate_square_root_of(int number) {
	   Anacleto.attemptsTo(Calculate.thesquareroot(number));
	}

	@Then("^the result should be (\\d+)$")
	public void the_result_should_be(int thisNumber) throws Exception {
	    Anacleto.can(seeThat(TheResult.ofTheSquareRoot(), is(thisNumber)));
	}

}
