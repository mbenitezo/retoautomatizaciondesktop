package desktop.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class CalculatorPage {
	
	public static final Target BUTTON_NINE = Target.the("Button 9")
			.locatedBy("//*[@AutomationId='num9Button']");
	
	public static final Target BUTTON_SQUAREROOT = Target.the("Button square root")
			.locatedBy("//*[@AutomationId='squareRootButton']");
	
	 public static final Target RESULT = Target.the("result")
	        .locatedBy("//*[@AutomationId='CalculatorResults']");
			
}
