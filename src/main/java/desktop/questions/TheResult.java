package desktop.questions;

import desktop.userinterfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the result of the sum")
public class TheResult implements Question<Integer>{

	@Override
	public Integer answeredBy(Actor actor) {
		System.out.println("Resultado  " + CalculatorPage.RESULT.resolveFor(actor).getText().replace("La pantalla muestra ", "").trim());
        return Integer.parseInt(CalculatorPage.RESULT.resolveFor(actor).getText().replace("La pantalla muestra ", "").trim());
	}

	public static TheResult ofTheSquareRoot() {
		return new TheResult();
	}
	
	

}
