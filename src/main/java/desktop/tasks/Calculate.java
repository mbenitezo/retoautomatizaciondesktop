package desktop.tasks;

import desktop.userinterfaces.CalculatorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Calculate implements Task {
	
	private int number;

	public Calculate(int number) {
		this.number = number;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(CalculatorPage.BUTTON_NINE));
		actor.attemptsTo(Click.on(CalculatorPage.BUTTON_SQUAREROOT));
	}

	public static Calculate thesquareroot(int number) {
		return Tasks.instrumented(Calculate.class, number);
	}

}
