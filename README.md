# RetoAutomatizacionDesktop

Para �ste reto se tiene en cuenta que se ha usado WinAppDriver (Windows Application Driver), el cual, es el integrador entre el desarrollo de la automatizaci�n y el programa de Windows.

Para correr �sta automatizaci�n, es importante contar con:

* Windows 10.
* WinAppDriver (Descargar archivo instalador msi aqu�: https://github.com/Microsoft/WinAppDriver/releases . Posiblemente debas habilitar el modo Programador para instalar dicho programa.
* Se usa el mapeador de objectos llamado Inspect.exe, el cual, puede ser descargado en la siguiente ruta: https://github.com/blackrosezy/gui-inspect-tool ), aunque existen otras herramientas que puedes usar.

Es importante resaltar que debes activar en Windows 10 la configuraci�n de Desarrollador, �sta activaci�n la puedes hacer por medio de los siguiente pasos:

1. Clic en Inicio.
2. Clic en Configuraci�n.
3. Luego clic en la opci�n Actualizaci�n y seguridad.
4. Clic en la opci�n "Para Programadores".
5. Clic en la opci�n "Modo 	de Programador"

En caso de que el sistema solicite la confirmaci�n de dicha acci�n, se acepta.

## Iniciar WinAppDriver

Si tu Windows 10 es de 64 bit:

1. Win + R (Ejecutar).
2. Copias y pegas la siguiente ruta: 
	"C:\Program Files (x86)\Windows Application Driver\WinAppDriver.exe"

Si todo sale bien, deber�as ver lo siguiente:

Windows Application Driver listening for requests at: http://127.0.0.1:4723/
Press ENTER to exit.

Realizado lo anterior ya puedes lanzar la automatizaci�n y si no funciona entonces algo habr� omitido y no me acuerdo porque tengo mucho sue�o.




